package com.example.compose_contacts

enum class SortType {
    FIRST_NAME,
    LAST_NAME,
    PHONE_NUMBER
}