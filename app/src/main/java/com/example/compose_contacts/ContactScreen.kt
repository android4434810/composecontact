package com.example.compose_contacts

import android.support.v4.os.IResultReceiver2.Default
import androidx.compose.foundation.clickable
import androidx.compose.foundation.horizontalScroll
import androidx.compose.foundation.layout.Arrangement
import androidx.compose.foundation.layout.Column
import androidx.compose.foundation.layout.Row
import androidx.compose.foundation.layout.fillMaxSize
import androidx.compose.foundation.layout.fillMaxWidth
import androidx.compose.foundation.layout.padding
import androidx.compose.foundation.lazy.LazyColumn
import androidx.compose.foundation.lazy.items
import androidx.compose.foundation.rememberScrollState
import androidx.compose.material.icons.Icons
import androidx.compose.material.icons.filled.Add
import androidx.compose.material.icons.filled.Delete
import androidx.compose.material.icons.filled.MoreVert
import androidx.compose.material3.DropdownMenu
import androidx.compose.material3.DropdownMenuItem
import androidx.compose.material3.FloatingActionButton
import androidx.compose.material3.Icon
import androidx.compose.material3.IconButton
import androidx.compose.material3.RadioButton
import androidx.compose.material3.Scaffold
import androidx.compose.material3.Text
import androidx.compose.runtime.Composable
import androidx.compose.runtime.getValue
import androidx.compose.runtime.mutableStateOf
import androidx.compose.runtime.remember

import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.text.font.FontWeight
import androidx.compose.ui.unit.dp
import androidx.compose.ui.unit.sp

@Composable
fun ContactScreen(
    state: ContactState,
    onEvent: (ContactEvent) -> Unit
){
    var expanded = remember { mutableStateOf(false) }
    Scaffold(
        floatingActionButton = {
            FloatingActionButton(onClick = { onEvent(ContactEvent.ShowDialog)}) {
             Icon(imageVector = Icons.Default.Add, contentDescription = "Add Contact")
            }

        }, modifier = Modifier.padding(16.dp)
    ) {paddingValues ->
        if(state.isAddingContact){
            AddContactDialog(state = state, onEvent = onEvent)
        }
        LazyColumn(
            contentPadding = paddingValues,
            modifier = Modifier.fillMaxSize(),
            verticalArrangement = Arrangement.spacedBy(16.dp)
        ) {
           item{
               Row (modifier = Modifier
                   .fillMaxWidth(),
                   verticalAlignment = Alignment.CenterVertically){
                   Text(text = "Contacts", Modifier.weight(1f), fontSize = 25.sp, fontWeight = FontWeight.SemiBold)
                   Column {
                       IconButton(onClick = {expanded.value=!expanded.value}) {
                           Icon(imageVector = Icons.Default.MoreVert, contentDescription = "Sort Types" )
                       }
                       if (expanded.value) {
                           DropdownMenu(
                               expanded = expanded.value,
                               onDismissRequest = { expanded.value = false }
                           ) {
                               SortType.entries.forEach {
                                   DropdownMenuItem(onClick = { onEvent(ContactEvent.SortContacts(it)) }, text ={
                                       Row (verticalAlignment = Alignment.CenterVertically){
                        RadioButton(selected = state.sortType==it, onClick = { onEvent(ContactEvent.SortContacts(it)) })
                           Text(text = it.name)
                       }
                                   } )
                               }
                           }
                       }
                   }


                   
//                   SortType.values().forEach {
//                       Row (modifier = Modifier.clickable {
//                           onEvent(ContactEvent.SortContacts(it))
//                       }, verticalAlignment = Alignment.CenterVertically){
//                        RadioButton(selected = state.sortType==it, onClick = { onEvent(ContactEvent.SortContacts(it)) })
//                           Text(text = it.name)
//                       }
//                   }
               }
           }
            items(state.contacts){
                Row(modifier = Modifier.fillMaxWidth()) {
                    Column (modifier = Modifier.weight(1f)){
                        Text(text = "${it.firstName} ${it.lastName}", fontSize = 20.sp)
                        Text(text = it.phoneNumber, fontSize = 12.sp)
                    }
                    IconButton(onClick = { onEvent(ContactEvent.DeleteContact(it)) }) {
                        Icon(imageVector = Icons.Default.Delete , contentDescription = "Delete Contact" )
                    }
                }
            }
        }

    }
}