package com.example.compose_contacts

sealed interface ContactEvent {

    object SaveContact:ContactEvent
    data class SetFirstName(val firstName: String):ContactEvent
    data class SetLastName(val  lastNAme:String):ContactEvent
    data class SetPhoneNumber(val phoneNumber:String):ContactEvent
    object ShowDialog: ContactEvent
    object HideDialog: ContactEvent
    data class SortContacts(val sortType: SortType):ContactEvent
    data class DeleteContact(val contact: Contact):ContactEvent
}